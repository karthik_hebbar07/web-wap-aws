import random
import sys
import time
import string
# from alright import WhatsApp
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    UnexpectedAlertPresentException,
    NoSuchElementException,
)
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.chrome.service import Service
# from subprocess import CREATE_NO_WINDOW


import requests


def sessionGenerator():

    BASE_URL = 'https://web.whatsapp.com/send?phone='
    chrome_options = Options()
    chrome_options.headless = True
    chrome_options.add_argument('--profile-directory=Default')
    chrome_options.add_argument(
         "user-agent=User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.binary_location = '/usr/bin/google-chrome'
    browser = webdriver.Chrome('/usr/bin/chromedriver',options=chrome_options)

    wait = WebDriverWait(browser, 600)
    wait_user = WebDriverWait(browser, 15)

    browser.get(BASE_URL)
  

    sent = 0
    unsent = 0
    numbers = [
        8861562478,
        9242137092,
        9110208429,
        8861562478,
        9242137092,
        9110208429,
        8861562478,
        9242137092,
        9110208429,
        8861562478,
        9242137092,
        9110208429,
        8861562478,
        9242137092,
        9110208429,
    ]

    message = "(AWS Message)from headless chrome This is a test message for testing purpose, please ignore"
    x_path = '//*[@id="main"]/footer/div[1]/div/span[2]/div/div[2]/div[1]/div/div[1]'
    # browser = webdriver.Chrome(ChromeDriverManager().install())
    # browser.get("https://web.whatsapp.com/")
    print("************** Waiting for QR code scan ***********")
    # time.sleep(5)
    print('generating screen shot name...')
    res = ''.join(random.choices(
        string.ascii_uppercase + string.digits, k=8))

    # QRcode
    time.sleep(20)
    res = str(time.time()) + res
    qr_result = ''
    try:
        print('Finding QR code')
        QR_CODE = browser.find_element(
            by=By.XPATH, value='//*[@id="app"]/div/div/div[2]/div[1]')
        time.sleep(3)
        image = QR_CODE.screenshot('qr_codes_wtsapp/'+res + '.png')

        print('QR code screen shot saved.')

        # sending to numasoftworks server
        image_path = 'qr_codes_wtsapp/'+res + '.png'

        test_file = open(image_path, "rb")
        test_response = requests.post(
            'https://numasoftworks.com/pillzy/api/image', files={"image": test_file})
        print(test_response.json())
        qr_result = True
    except:
        qr_result = False

    if qr_result:
        print('screen shot recieved')
    else:
        print('screenshot not receieved')

    time.sleep(15)
    for number in numbers:
        user_num = '91' + str(int(number))

        link = f'{BASE_URL}{user_num}'
        browser.get(link)
        try:

            input_box = WebDriverWait(browser, 15).until(
                EC.presence_of_element_located((By.XPATH, x_path)))

            input_box.send_keys(message, Keys.ENTER)
            time.sleep(5)
            sent += 1
            print(f"Message sent successfuly to "+user_num)

        except Exception as e:
            print(e)
            print("message sending failed for "+user_num)
            unsent += 0
            time.sleep(120)
    print('-------Report----------')
    print('Total Numbers: '+str(len(numbers)))
    print('Sent Numbers: '+str(sent))
    print('Unsent Numbers: '+str(unsent))
    # print(session)
    time.sleep(10)
    # browser.close()


sessionGenerator()
